/**
 * Created by AH-Youker on 17/6/27.
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	config.height = '500px';
    config.uiColor = '#AADC6E';
    config.fontSize_defaultLabel = '40px';
    config.image_previewText="";
	config.removeButtons = 'About';
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.filebrowserUploadUrl="/ckeditorUpload";
    CKEDITOR.tools.callFunction()

};