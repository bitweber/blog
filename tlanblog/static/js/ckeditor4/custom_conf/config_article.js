/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
$(document).ready(function () {
    CKEDITOR.replace('id_content');
    CKEDITOR.config.width = '100%';
});



CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	config.height = '600px';
    config.uiColor = '#AADC6E';
};
