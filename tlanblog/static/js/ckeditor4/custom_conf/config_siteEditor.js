/**
 * Created by AH-Youker on 17/6/27.
 */
$(document).ready(function () {
    CKEDITOR.replace('id_about_info');
    CKEDITOR.config.width = '100%';
});



CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	config.height = '600px';
    config.uiColor = '#AADC6E';
};