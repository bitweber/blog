/**
 * Created by AH-Youker on 17/6/23.
 */

$(document).ready(function () {
    //分类目录索引
    $(".categ_list li").click(function () {
        var type = "category";
        var keywords = $(this).attr('title');
        window.location.href = "/assort/?type="+type+"&keywords="+keywords;
    });
    //日期索引
    $(".date_list li").click(function () {
        var type = "datetime";
        var keywords = $(this).attr('title');
        window.location.href = "/assort/?type="+type+"&keywords="+keywords;
    });
    //搜索框索引提示
    $("input#searchText").keyup(function () {
        var type = "search";
        var keywords = $(this).val();
        //initArticlePage(type,page,keywords);
    });
    $("input#searchText").keydown(function (event) {
		if(event.keyCode == 13){
			var type = "search";
			var keywords = $("input#searchText").val();
			window.location.href = "/assort/?type="+type+"&keywords="+keywords;
		}
    });
    //搜索按钮
    $(".search-button").click(function () {
        var type = "search";
        var keywords = $("input#searchText").val();
        window.location.href = "/assort/?type="+type+"&keywords="+keywords;
        //initArticlePage(type,page,keywords);
    });
    //标签索引
    $("#tags label").click(function () {
        var type = "tag";
        var keywords = $(this).text();
        window.location.href = "/assort/?type="+type+"&keywords="+keywords;
    });
});

//page init
function initArticlePage(type,keywords) {
    $(function(){
    		$(".col-md-8").pageFun({  /*在本地服务器上才能访问哦*/
    			interFace:"/ajax/arts/",  /*接口*/
                paraData :{'type':type, 'keywords':keywords},
    			displayCount:10,  /*每页显示总条数*/
    			maxPage:6,/*每次最多加载多少页*/
    			dataFun:function(data){
    				var htmlstr = "";
					htmlstr += "<article class=\"art-item\">";
					htmlstr += "<div class=\"art_item_img\">";
					htmlstr += "<a href=\"" + data.url + "\"><img src=\"" + data.head_img + "\" alt=\"" + data.title + "\"></a>";
					htmlstr += "</div>";
					htmlstr += "<div class=\"art_item_info\">";
					htmlstr += "<dl>";
					htmlstr += "<dd>";
					htmlstr += "<h2><a href=\""+data.url+"\">" + data.title + "</a></h2>";
					htmlstr += "</dd>";
					htmlstr += "<dd class=\"art_item_desc\">";
					htmlstr += "<p>" + data.desc + "</p>";
					htmlstr += "</dd>";
					htmlstr += "</dl>";
					htmlstr += "</div>";
					htmlstr += "<div class=\"art_item_meta\"><span style=\"float: left;text-align: center;display: block;padding-left: 5px;font-weight: 400;padding-right: 5px;background-color: #c7ddef\"><a>" + data.category + "</a></span><i class=\"fa fa-user-circle-o\"></i><a href=\"/about\">" + data.user + "</a><span></span><i class=\"fa fa-clock-o\"></i>" + data.pub_date + "<span></span><span><i class=\"fa fa-eye\"></i>" + data.click_count + "</span><span></span><i class=\"fa fa-comment-o\"></i><a href=\"" + data.url + "#input_comment\">" + data.comments_count + "</a></span>";
					htmlstr += "</div>";
					htmlstr += "</article>";
    				return htmlstr;
    			},
    			pageFun:function(i){
    				var pageHtml = '<li class="pageNum">'+i+'</li>';
					return pageHtml;
    			}
    		})
    	})
}

//URL参数解析
function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg);  //匹配目标参数
        if (r != null) return decodeURI(r[2]); return null; //返回参数值
}


