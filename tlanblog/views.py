# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.template.loader import get_template
#from django.core.paginator import Paginator,InvalidPage,EmptyPage,PageNotAnInteger
from .models import *
from django.db.models import Q
from tlanblog.form import CommentForm
import json
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

def login_interface(request):
    return render(request,'user.html')
# 主页
def index(request):
     slider_as = SliderArticle.objects.all()[:4]
     context = base_data_proc(request,{'slider_as': slider_as})
     return render(request, 'index.html', context=context)

#搜索页
def assort(request):
    context = base_data_proc(request)
    return render(request,'search.html',context)

#关于
def about(request):
    about = SiteManage.objects.get(id = 1)
    context = base_data_proc(request, {'about':about})
    return render(request,'about.html', context)

#图片轮转
def slider_context(request, slider_id):
    article = SliderArticle.objects.get(id = slider_id)
    article.click_count+=1
    article.save()
    context = base_data_proc(request, {'article': article})
    return render(request, 'article.html',context)

def comment_add(request):
    if request.is_ajax() and request.method == 'POST':
        #print request.POST
        pid_str = request.POST['pid']
        pid = None
        if pid_str != "" and pid_str != None:
            try:
                pid_id = int(pid_str)
                pid = Comment.objects.get(id=pid_id)
            except Exception, e:
                print e.message
                pid = None
        else:
            pid = None
        oHfName = request.POST['oHfName']
        content = request.POST['content']
        article_id = int(request.POST['article'])
        article = Article.objects.get(id = article_id)
        user_ip = request.META['REMOTE_ADDR']
        if content != None or content != "":
            comment = Comment.objects.create(pid=pid, article=article, content=content, pub_date=timezone.now(), user_ip=user_ip, atName=oHfName)
            username = "游客 ***."+user_ip.split('.')[1] + ".***."+ user_ip.split('.')[-1]
            comment.username = username
            comment.save()
            #print username
            comment_json = {}
            comment_json['hfName'] = comment.username
            comment_json['hfTime'] = comment.pub_date.strftime("%Y-%m-%d %H:%M:%S")
            comment_json['atName'] = comment.atName
            comment_json['like'] = comment.like
            comment_json['id'] = comment.id
            #print comment_json['id']
            return HttpResponse(json.dumps(comment_json), content_type='application/json')
    raise Http404

def comment_like(request):
    if request.is_ajax() and request.method=='POST':
        comment_id = request.POST['comment_id']
        likes_num = request.POST['like']
        if comment_id.strip!="" and int(comment_id.strip()) and int(likes_num):
            id = int(comment_id)
            comment = Comment.objects.get(id=id)
            if comment!= None:
                comment.like = int(likes_num)
                comment.save()
                json_like = {'like':comment.like}
                return HttpResponse(json.dumps(json_like),content_type='application/json')
    return Http404

def comment_del(request):
    if request.is_ajax() and request.method=='POST':
        comment_id = request.POST['comment_id']
        req_ip = request.META['REMOTE_ADDR']
        if comment_id.strip() != "" and int(comment_id.strip()):
            id = int(comment_id.strip())
            comment = Comment.objects.get(id=id)
            if(comment!=None and req_ip==comment.user_ip):
                comment.delete()
                is_ok = '1'
                json_ok = {'is_ok':is_ok}
                return HttpResponse(json.dumps(json_ok),content_type='application/json')
    return Http404

#文章内容
def article_context(request, article_id):
    comment_form = CommentForm
    all_arts = Article.objects.all()
    total_num = all_arts.count()
    article = all_arts.get(id = article_id)
    comments = Comment.objects.filter(article= article).order_by('id')
    article_id = int(article_id)
    article_pre_id = article_id-1
    article_next_id = article_id+1
    article_pre = None
    article_next = None
    if article_pre_id > 0 and article_pre_id<=total_num:
        article_pre = all_arts.get(id = article_pre_id)
    if article_next_id >0 and article_next_id<=total_num:
        article_next = all_arts.get(id = article_next_id)
    article.click_count+=1
    article.save()
    #评论帅选
    comment_list = []
    for comment in comments:
        for item in comment_list:
            if not hasattr(item, 'child_comment'):
                setattr(item, 'child_comment', [])
            if comment.pid == item:
                item.child_comment.append(comment)
        if comment.pid is None:
            comment_list.append(comment)

    context = base_data_proc(request,{'comment_form':comment_form,'comments': comment_list,'article': article,'article_pre': article_pre, 'article_next': article_next})
    return render(request, 'article.html',context)

#日期分组函数
def date_groups_get():
    date_groups = Article.objects.raw(
        'SELECT id, pub_date as pub_time, COUNT(*) as count FROM tlanblog_article GROUP BY strftime("%Y-%m", pub_date) ORDER BY pub_date DESC'
    )
    return date_groups


#基础数据返回
def base_data_proc(request, context_dict={}):
    site_title = SiteManage.objects.get(id=1).main_title
    tags = Tag.objects.all()
    categorys = Category.objects.all().filter()
    articles_rec = Article.objects.all().filter(is_recommend=True)[:5]
    date_category = date_groups_get()
    context = {
        'site_title': site_title,
        'tags': tags,
        'categorys': categorys,
        'articles_rec': articles_rec,
        'date_category': date_category,
        'ip_address': request.META['REMOTE_ADDR'],
    }
    context.update(context_dict)
    return context

def ajax_articles(request):
    if request.is_ajax() and request.GET:
        type = request.GET.get('type')
        keywords = request.GET.get('keywords')
        articles = None
        if type=="index":
            articles = Article.objects.all()
        elif type=="category":
            articles = Article.objects.all().filter(category__name=keywords)
        elif type=="datetime":
            date_s = keywords.split('-')
            try:
                if len(date_s) == 2 and int(date_s[0] and int(date_s[1])):
                    articles = Article.objects.all().filter(pub_date__year=date_s[0]).filter(pub_date__month=date_s[1])
                else:articles = None
            except Exception,e:
                print '异常：'+e.message
                articles = None
        elif type=="tag":
            articles = Article.objects.all().filter(tag__name=keywords)
        elif type=="search":
            articles = Article.objects.all().filter(
                Q(title__contains=keywords) |
                Q(desc__contains=keywords) |
                Q(content__contains=keywords) |
                Q(category__name__contains=keywords) |
                Q(tag__name__contains=keywords)
            ).distinct()

        else:
            print '数据类型错误，查询失败！'
            raise Http404
        jsonstr = convert_to_jsontype(type, articles)
        return HttpResponse(jsonstr, content_type='application/json')
    else:
        raise Http404

#将文章基本信息转化为json返回字符串
def convert_to_jsontype(type, articles):
    result = {}
    result['type'] = type
    result['status'] = "ok"
    result['count'] = len(articles)
    art_list = []
    for art in articles:
        art_json = {}
        art_json['url'] = "/article/%s/%s/" %(art.pub_date.strftime("%Y%m"),art.id)
        art_json['title'] = art.title
        art_json['user'] = art.user.alias
        art_json['head_img'] = art.head_img.url
        art_json['pub_date'] = art.pub_date.strftime("%Y-%m-%d %H:%M")
        art_json['desc'] = art.desc
        art_json['category'] = art.category.name
        art_json['comments_count'] = art.comment_set.count()
        art_json['click_count'] = art.click_count
        art_list.append(art_json)

    result['articles'] = art_list
    return json.dumps(result)