# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
# Create your models here.
#用户模型
class User(AbstractUser):
    alias = models.CharField(max_length=8,default="匿名",verbose_name="昵称")
    avatar = models.ImageField(upload_to="avatar/%Y/%m/%d",default="avatar/default.png",max_length=200,verbose_name="头像")
    qq = models.CharField(max_length=15,blank=True,null=True,verbose_name="QQ号码")
    mobile = models.CharField(max_length=11,blank=True,null=True,unique=True,verbose_name="手机号码")

    class Meta:
        verbose_name = "用户"
        verbose_name_plural = "用户管理"
        ordering = ['-id']

    def __str__(self):
        return self.username

# 标签模型
class Tag(models.Model):
    name = models.CharField(max_length=6, verbose_name="标签名")
    index = models.IntegerField(default=999, verbose_name="标签排序")
    tag_add_date = models.DateTimeField(auto_now=True,verbose_name="添加时间")
    class Meta:
        verbose_name = "标签"
        verbose_name_plural = "标签管理"

    def __unicode__(self):
        return self.name

# 分类模型
class Category(models.Model):
    name = models.CharField(max_length=6, verbose_name="分类名称")
    index = models.IntegerField(default=999, verbose_name="分类排序")
    c_add_date = models.DateTimeField(auto_now_add=True,verbose_name="添加时间")
    class Meta:
        verbose_name = "分类"
        verbose_name_plural = "分类管理"
        ordering = ['-c_add_date']

    def __unicode__(self):
        return self.name

#文章模型
class Article(models.Model):
    title = models.CharField(verbose_name="文章标题",max_length=50)
    head_img = models.ImageField(upload_to="article/uploads/images/%Y/%m",default="article/default.png",verbose_name="文章头图")
    desc = models.CharField(verbose_name="文章描述",max_length=200)
    content = models.TextField(verbose_name="文章内容")
    click_count = models.IntegerField(default=0,verbose_name="点击次数")
    is_recommend = models.BooleanField(default=False, verbose_name="是否推荐")
    pub_date = models.DateTimeField(default=timezone.now,verbose_name="发布时间")
    user = models.ForeignKey(User, verbose_name="发布者")
    category = models.ForeignKey(Category,blank=True,null=True,verbose_name="文章分类")
    tag = models.ManyToManyField(Tag,verbose_name="标签")

    class Meta:
        verbose_name = "文章"
        verbose_name_plural="文章管理"
        ordering = ['-pub_date']

    def __unicode__(self):
        return self.title

# 评论模型
class Comment(models.Model):
    pub_date = models.DateTimeField(auto_now_add=True, verbose_name="评论时间")
    content = models.TextField(max_length=500, verbose_name="评论内容")
    username = models.CharField(default='匿名',max_length=20, verbose_name="评论者")
    article = models.ForeignKey(Article, blank=True, null=True, verbose_name="文章")
    pid = models.ForeignKey('self', blank=True, null=True, verbose_name="父级评论")
    user_ip = models.GenericIPAddressField(default="0.0.0.0",verbose_name="用户IP")
    like = models.IntegerField(default=0,verbose_name="点赞数")
    atName = models.CharField(default="", max_length=20,verbose_name="@对象")
    class Meta:
        verbose_name = "评论"
        verbose_name_plural = "评论管理"
        ordering = ['-pub_date']

    def __unicode__(self):
        return str(self.id)

#大图文章
class SliderArticle(Article):
    index = models.IntegerField(verbose_name="大图排序")

    class Meta:
        verbose_name = "大图文章"
        verbose_name_plural = "大图文章"
        ordering = ['-pub_date']

    def __unicode__(self):
        return self.title

#站点信息管理
class SiteManage(models.Model):
    main_title = models.CharField(max_length=20,verbose_name="网站标题")
    change_date = models.DateTimeField(default=timezone.now, verbose_name="修改时间")
    about_title = models.CharField(max_length=30, default="关于博主",verbose_name="关于博主标题")
    about_info = models.TextField(max_length=1000, default="博主信息",verbose_name="关于内容")
    class Meta:
        verbose_name = "网页"
        verbose_name_plural = "网页管理"
        ordering = ['-change_date']
    def __unicode__(self):
        return self.main_title
