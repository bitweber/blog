#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 17/6/28 下午4:18
# @Author  : Tlan
# @Site    : 
# @File    : form.py
# @Software: PyCharm

from django.forms import ModelForm, Textarea
from tlanblog.models import Comment
from django.utils.translation import ugettext_lazy as __

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['content']
        labels = {
            'content':'',
        }
        widgets = {
            'content': Textarea(attrs={'class':'comment-content', 'name':'comment', 'cols':80, 'rows':5,'id':'comment', 'style':'font-size:16px;resize:none;margin:6px 0 0 3px;'})
        }
        error_messages = {
            'content':{
                'max_length': __('字数限制500，请重新输入！'),
            },
        }
