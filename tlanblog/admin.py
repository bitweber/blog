# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import *

# 大图文章后台管理
@admin.register(SliderArticle)
class SliderAdmin(admin.ModelAdmin):
    list_display = ['title', 'desc', 'click_count', 'is_recommend', 'category', 'user', 'pub_date']

    fieldsets = (
        ('基本内容', {
            'fields': ('title', 'desc', 'head_img', 'index', 'is_recommend', 'user', 'pub_date', 'category', 'tag')
        }),
        ('主要内容', {
            'fields': ('content',)
        }),
    )

    class Media:
        css = {
            'all': ('/static/js/ckeditor4/skins/global.css',)
        }
        js = (
            '/static/js/jquery.js',
            '/static/js/ckeditor4/config.js',
            '/static/js/ckeditor4/ckeditor.js',
            '/static/js/ckeditor4/plugins/image/dialogs/image.js',
            '/static/js/ckeditor4/custom_conf/config_article.js',
        )

# 用户后台管理
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username','password','email','qq','mobile','is_staff','is_active','date_joined','last_login']

# 文章后台管理
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title','desc','click_count','is_recommend','category','user','pub_date']
    fieldsets = (
        ('基本内容',{
            'fields': ('title', 'desc', 'head_img','is_recommend','user','pub_date','category','tag')
        }),
        ('主要内容',{
            'fields': ('content',)
        }),
    )

    class Media:
        css = {
            'all': ('/static/js/ckeditor4/skins/global.css',)
        }
        js = (
            '/static/js/jquery.js',
            '/static/js/ckeditor4/config.js',
            '/static/js/ckeditor4/ckeditor.js',
            '/static/js/ckeditor4/custom_conf/config_article.js',
        )

# 分类后台管理
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name','index','c_add_date']

# 标签后台管理
@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name','index','tag_add_date']

# 评论后台管理
@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['content','article','pub_date']

# 网页信息后台管理
@admin.register(SiteManage)
class SiteManageAdmin(admin.ModelAdmin):
    list_display = ['main_title','change_date']

    class Media:
        css = {
            'all': ('/static/js/ckeditor4/skins/global.css',)
        }
        js = (
            '/static/js/jquery.js',
            '/static/js/ckeditor4/config.js',
            '/static/js/ckeditor4/ckeditor.js',
            '/static/js/ckeditor4/plugins/image/dialogs/image.js',
            '/static/js/ckeditor4/custom_conf/config_siteEditor.js',
        )


