# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-22 16:58
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('tlanblog', '0012_auto_20170621_1953'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='slug',
            field=models.SlugField(auto_created=True, default=datetime.datetime(2017, 6, 22, 16, 58, 58, 554956, tzinfo=utc), unique=True, verbose_name='\u6587\u7ae0\u6807\u8bc6'),
        ),
    ]
