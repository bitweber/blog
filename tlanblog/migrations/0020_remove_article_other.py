# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-22 17:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tlanblog', '0019_article_other'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='other',
        ),
    ]
