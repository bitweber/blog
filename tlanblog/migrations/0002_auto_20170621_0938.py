# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-21 09:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tlanblog', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ['-pub_date'], 'verbose_name': '\u6587\u7ae0\u7ba1\u7406', 'verbose_name_plural': '\u6587\u7ae0'},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['-pub_date'], 'verbose_name': '\u5206\u7c7b\u7ba1\u7406', 'verbose_name_plural': '\u5206\u7c7b\u7ba1\u7406'},
        ),
        migrations.AddField(
            model_name='category',
            name='pub_date',
            field=models.DateTimeField(auto_now=True, verbose_name='\u6dfb\u52a0\u65f6\u95f4'),
        ),
    ]
