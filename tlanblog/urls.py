#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 17/6/19 下午12:52
# @Author  : Aries
# @Site    : 
# @File    : urls.py
# @Software: PyCharm

from django.conf.urls import url,include
from django.conf.urls.static import static

from . import views
from django.contrib import admin
from blog import settings
from blog.upload import upload_img

urlpatterns = [
    url(r'^$', views.index),
    url(r'^assort/$', views.assort),
    url(r'^sliders/(\d{1,6})/(?P<article_id>(\d{1,10}))/$', views.article_context),
    url(r'^article/(\d{1,6})/(?P<article_id>(\d{1,10}))/$', views.article_context),
    url(r'^ajax/arts/$', views.ajax_articles),
    url(r'^comment/add/$',views.comment_add),
    url(r'^comment/del/$',views.comment_del),
    url(r'^comment/like/$',views.comment_like),
    url(r'^ckeditorUpload$',upload_img),
    url(r'^about/$', views.about),
    url(r'^login/$',views.login_interface),
    url(r'^manager/', admin.site.urls),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)