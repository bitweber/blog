# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TlanblogConfig(AppConfig):
    name = 'tlanblog'
    verbose_name = u"我的博客"
