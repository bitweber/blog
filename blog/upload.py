#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 17/6/27 下午7:36
# @Author  : Tlan
# @Site    : 
# @File    : upload.py
# @Software: PyCharm

from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import os
import uuid

@csrf_exempt
def upload_img(request):
    # print request.FILES['upload'].name
    callback = request.GET.get('CKEditorFuncNum')
    if request.method == 'POST':
        file =  request.FILES['upload']
        # print type(file.name)
        if file:
            img_allow_suffix = ['jpg', 'png', 'jpeg', 'gif', 'bmp']
            flash_allow_suffix = ['swf']
            file_suffix = file.name.split('.')[-1]
            if file_suffix in img_allow_suffix:
                backData = return_data(file, file_suffix,callback)
                return HttpResponse(backData)
            elif file_suffix in flash_allow_suffix:
                backData = return_data(file, file_suffix,callback)
                return HttpResponse(backData)
    return HttpResponse(None)


# 设置返回数据
def return_data(f, f_suffix,cbk):
    filename = str(uuid.uuid1()) + "." + f_suffix
    file_path = os.path.join(settings.CKEDITOR_UPLOAD_PATH, filename)
    file_upload(f, file_path)
    # server_host = request.get_host()
    imagedir = file_path.replace(settings.BASE_DIR, "")
    # imageurl = "http://" + server_host + imagedir
    backData = "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(" + cbk + ",'" + imagedir + "',''" + ")" + "</script>"
    return backData

# 文件写入
def file_upload(f, path):
    image = open(path,'wb+')
    for chunk in f.chunks():
        image.write(chunk)
    image.close()

